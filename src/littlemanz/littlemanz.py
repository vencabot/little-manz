from random import shuffle
from time import sleep
from typing import List


class Ability:
    def __init__(
            self, label: str="Ability",
            desc: str="Use an ability.") -> None:
        self.label = label
        self.desc = desc

    def get_targets(self, user: "Hero") -> List["Hero"]:
        return []

    def execute(self, user: "Hero", targets: List["Hero"]) -> str:
        pass

    def get_output_str(self, user: "Hero", targets: List["Hero"]) -> str:
        if user in targets:
            targets.remove(user)
            targets.append(user)
        target_str = ""
        for target_index in range(len(targets)):
            if len(targets) > 2 and target_index > 1:
                target_str += ","
            if len(targets) > 1:
                target_str += " "
                if target_index == len(targets) - 1:
                    target_str += "and "
            label = targets[target_index].label
            if label == user.label:
                label = "themselves"
            target_str += label
        target_str = target_str if target_str else "no one"
        return f"{user.label} uses {self.label} on {target_str}."


class HeroAttr:
    def __init__(self, ceil: int, now: int) -> None:
        self.ceil = ceil
        self.now = now
        self.init = now


class Hero:
    def __init__(
            self, label: str="Hero", desc: str="A generic hero.",
            hp: int=10, atk: int=3, con: int=0) -> None:
        self.label = label
        self.desc = desc
        self.hp = HeroAttr(hp, hp)
        self.atk = HeroAttr(99, atk)
        self.con = HeroAttr(99, con)
        self.allies = []
        self.enemies = []

    def get_status_str(self) -> str:
        return (
                f"{self.label} - HP: {self.hp.now}/{self.hp.ceil} | "
                f"ATK: {self.atk.now} | CON: {self.con.now}")

    def use_ability(self, ability: Ability) -> None:
        targets = ability.get_targets(self)
        living_targets = [target for target in targets if target.hp.now > 0]
        ability.execute(self, targets)
        print(f"    {ability.get_output_str(self, targets)}")
        for target in living_targets:
            if target.hp.now <= 0:
                print(f"    {target.label} is knocked out!")
        for target in targets:
            print(f"    {target.get_status_str()}")

    def act(self) -> None:
        pass


class Team:
    def __init__(self, label: str, desc: str, heroes: List[Hero]) -> None:
        self.label = label
        self.desc = desc
        self.heroes = heroes


class Turn:
    def __init__(self, hero: Hero) -> None:
        self.hero = hero

    def execute(self) -> None:
        if self.hero.hp.now > 0:
            self.hero.act()
            print()


class Phase:
    def __init__(self, teams: List[Team]) -> None:
        self.teams = teams

    def execute(self) -> None:
        turns = [Turn(hero) for team in self.teams for hero in team.heroes]
        shuffle(turns)
        for turn in turns:
            turn.execute()
            sleep(2.5)


class Battle:
    def __init__(self, teams: List[Team]) -> None:
        self.active_teams = teams
        self.knocked_out_teams = []

    def assign_enemies(self) -> None:
        for team in self.active_teams:
            enemy_teams = [x for x in self.active_teams if x is not team]
            for hero in team.heroes:
                for enemy in [x for y in enemy_teams for x in y.heroes]:
                    hero.enemies.append(enemy)

    def print_teams(self) -> None:
        for team in self.active_teams:
            print(f"{team.label}:")
            for hero in team.heroes:
                print(f"    {hero.get_status_str()}")
            print()

    def print_standings(self) -> None:
        standings = self.knocked_out_teams.copy()
        standings.extend(self.active_teams)
        standings.reverse()
        for placement in range(len(standings)):
            print(f"#{placement + 1}: {standings[placement].label}")
            for hero in standings[placement].heroes:
                print(f"    {hero.get_status_str()}")
            print()

    def pre_battle(self) -> None:
        self.assign_enemies()
        print("THE BATTLE IS ABOUT TO BEGIN.\n")
        self.print_teams()

    def post_battle(self) -> None:
        print("THE BATTLE HAS BEEN DECIDED.\n")
        self.print_standings()

    def handle_knockouts(self) -> None:
        for team in self.active_teams.copy():
            living = [hero for hero in team.heroes if hero.hp.now > 0]
            if not living:
                print(f"    {team.label} is completely routed!\n")
                self.active_teams.remove(team)
                self.knocked_out_teams.append(team)
 
    def execute(self) -> None:
        self.pre_battle()
        phase_counter = 0
        while len(self.active_teams) > 1:
            phase_counter += 1
            print(f"PHASE #{phase_counter}")
            phase = Phase(self.active_teams)
            phase.execute()
            self.handle_knockouts()
        self.post_battle()
