from random import choice
from typing import List

from src.littlemanz.littlemanz import Ability, Hero, Team, Battle


class Attack(Ability):
    def __init__(self) -> None:
        super().__init__("Punch")

    def get_targets(self, user: Hero) -> List[Hero]:
        living_enemies = [hero for hero in user.enemies if hero.hp.now > 0]
        return [] if not living_enemies else [choice(living_enemies)]

    def execute(self, user: Hero, targets: List[Hero]) -> None:
        if not targets:
            return
        target = targets[0]
        target.hp.now = target.hp.now - (user.atk.now - target.con.now)
        target.hp.now = target.hp.now if target.hp.now >= 0 else 0


class GetHyped(Ability):
    def __init__(self) -> None:
        super().__init__("Get Hyped")

    def get_targets(self, user: Hero) -> List[Hero]:
        return [user]

    def execute(self, user: Hero, targets: List[Hero]) -> None:
        if user.atk.now >= user.atk.ceil:
            return
        user.atk.now += 1


class PolishArmor(Ability):
    def __init__(self) -> None:
        super().__init__("Polish Armor")

    def get_targets(self, user: Hero) -> List[Hero]:
        return [user]

    def execute(self, user: Hero, targets: List[Hero]) -> None:
        if user.con.now >= user.con.ceil:
            return
        user.con.now += 1


class EatVitamin(Ability):
    def __init__(self) -> None:
        super().__init__("Eat Vitamin")

    def get_targets(self, user: Hero) -> List[Hero]:
        return [user]

    def execute(self, user: Hero, targets: List[Hero]) -> None:
        if user.hp.now >= user.hp.ceil:
            return
        user.hp.now += 3
        if user.hp.now > user.hp.ceil:
            user.hp.now = user.hp.ceil


class Warrior(Hero):
    def act(self):
        abilities = [
                Attack(), Attack(), Attack(), GetHyped(), PolishArmor(),
                EatVitamin()]
        self.use_ability(choice(abilities))


def main() -> None:
    venca = Warrior("Venca")
    dixxucker = Warrior("Dixxucker")
    bluerazz = Warrior("BlueRazz")
    nukepool = Warrior("Nukepool")
    team_v = Team("Team V", "Vencabot's Team", [venca, dixxucker])
    team_b = Team("Team B", "BlueRazz's Team", [bluerazz, nukepool])
    Battle([team_v, team_b]).execute()


if __name__ == "__main__":
    main()
